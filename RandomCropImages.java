import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;

public class RandomCropImages {
	
	private static final int numSamples = 150;
	private static final double maxRandom = 1.25;
	private static final double maxAngle = 0.05;

	private static final String basedir = "C:\\Users\\Stefan\\Documents\\HackaTUM\\train";
	private static final String resdir = "C:\\Users\\Stefan\\Documents\\HackaTUM\\results6";
	private static final String metadata = "metadata.txt";
	
	public static void main(String[] args) throws Exception {
		
		String[] logodirs = new File(basedir).list();
		
		for(String logodir : logodirs) {
			String [] subfolders = new File(basedir + "\\" + logodir).list();
			for(String logo : subfolders) {
				// Processing for each subfolder, skip if already done
				File destdir = new File(resdir + "\\" + logodir + "\\" + logo);
				if(!destdir.exists()) {
					destdir.mkdirs();
					processFolder(basedir + "\\" + logodir + "\\" + logo, resdir + "\\" + logodir + "\\" + logo);
				}
			}
		}
		
		
	}
	
	public static void processFolder(String dir, String resultDir) throws IOException {
		String metadataFile = dir + "\\" + metadata;
		
		if(new File(metadataFile).exists()) {
			String[] files = new File(dir).list();
			
			// Read Metadata from file
			Scanner scanner = new Scanner(new File(metadataFile));
			String format = null;
			try {
				format = scanner.nextLine();
			} finally {
				scanner.close();
			}
			String[] formats = format.split(",");
			int x = Integer.parseInt(formats[1]);
			int y = Integer.parseInt(formats[2]);
			int width = Integer.parseInt(formats[3]);
			int height = Integer.parseInt(formats[4]);
			
			
			for(int i = 0; i < numSamples; i++) {
				// Select random file
				String file = files[(int)(Math.random() * files.length)];
				
				// Random margins
				double margin1 = Math.random() * width * maxRandom;
				double margin2 = Math.random() * height * maxRandom;
				int x2 = (int)(x - margin1);
				int y2 = (int)(y - margin2);
				int width2 = (int)(width + margin1 + Math.random() * width * maxRandom);
				int height2 = (int)(height + margin2 + Math.random() * height * maxRandom);
				
				// Random rotation
				double angle = (Math.random() - 0.5) * 2 * maxAngle;
				
				if(file.endsWith(".jpg")) {
					BufferedImage image = ImageIO.read(new File(dir + "\\" + file));
					BufferedImage result = new BufferedImage(width2, height2, BufferedImage.TYPE_INT_RGB);
					Graphics2D g2 = result.createGraphics();
					
					// Translate and Rotate
					AffineTransform transform = new AffineTransform();
					transform.rotate(angle, width2 * 0.5, height2 * 0.5);
					transform.translate(-x2, -y2);
					
					g2.setTransform(transform);
					g2.drawImage(image, 0, 0, null);
					
					String filename = String.format("%04d", i);
					filename = resultDir + "\\" + filename + ".jpg";
					ImageIO.write(result, "jpg", new File(filename));
				}
			}
		}
	}

}
